from car import Car
import random
import doctest

# Global variables

MAX_TICKS = 100
NR_CARS = 5
MAX_FUEL = 10

cars_stopped = []
cars_onroad = []
cars_queue = []


# Ensure that the number of cars created is no less than 5
def create_dict_cars(dict_cars, nr_cars: int, randfuel: tuple, randfuel_T: tuple, randtime_T: tuple,
                     cars_onroad: list) -> int:
    """
    Keep values in the dictionary and return the number of cars created (length of the dictionary)
    :param nr_cars:
    :param randfuel:
    :param randfuel_T:
    :param randtime_T:
    :param cars_onroad:
    :return: return the number of cars in the dictionary
    >>> create_dict_cars({}, 0, (6, 10), (0, 0), (2, 4), []) # cria a lista com nr_cars == 0, são criados 5 carros
    5

    >>> create_dict_cars({}, 6, (6, 10), (0, 0), (2, 4), []) # cria a lista com nr_cars == 6, são criados 6 carros
    6

    >>> create_dict_cars({}, -2, (6, 10), (0, 0), (2, 4), []) # cria a lista com nr_cars == -1, são criados 5 carros
    5
    """

    state = "onroad"
    if nr_cars <= 5:
        nr_cars = 5
    for id in range(nr_cars):
        fuel = random.randint(randfuel[0], randfuel[1])
        fuel_threshold = random.randint(randfuel_T[0], randfuel_T[1])
        waiting_time_threshold = random.randint(randtime_T[0], randtime_T[1])
        dict_cars[id] = Car(id, state, fuel, fuel_threshold, waiting_time_threshold)
        cars_onroad.append(id)

    return len(dict_cars)


# Ensure that the fuel doesn't go negative
def reduce_fuel(car: Car, value: int) -> int:
    """
    Decrement fuel of the car.
    :param car:
    :param value:
    :return: retorna a quantidade de combustivel restante no carro

    >>> reduce_fuel(Car(0,"stopped",0,5,5),5)
    0

    >>> reduce_fuel(Car(1,"fuelstation",1,5,5),5)
    0

    >>> reduce_fuel(Car(3,"onroad",3,5,5),3)
    0

    >>> reduce_fuel(Car(4,"onroad",4,5,5),3)
    1

    >>> reduce_fuel(Car(4,"onroad",4,5,5),-3)   # decrementa-se o combustivel por um valor negativo, optou-se por incrementar pelo valor absoluto
    1

    """
    car.reduceFuel(abs(value))      # garante que há sempre um decremento de combustivel, mesmo com dFuel negativos
    return max(car.getFuel(), 0)    # garante que não há quantidades de combustivel negativas


# Ensure that all the cars that increment the waiting_time are in the state "fuelstation"
def increment_waiting_time(car: Car, value) -> bool:
    """
    Incrementing waiting time only for cars in state "fuelstation"
    :param car:
    :param value:
    :return:

    >>> increment_waiting_time(Car(4,"onroad",4,5,5),3)     # o carro está na estrada, não deverá incrementar
    False

    >>> increment_waiting_time(Car(4,"fuelstation",2,5,5),3)    # pode incrementar
    True

    >>> increment_waiting_time(Car(4,"stopped",0,5,5),3)        # o carro está parado, não incrementar
    False

    >>> increment_waiting_time(Car(4,"fuelstation",0,5,5),-3)   # foi atribuido um valor negativo ao incremento, incrementa-se pelo volor absoluto
    True

    """
    initial = car.getWaitingTime()
    if car.getState() == "fuelstation":
        car.incrementWaitingTime(abs(value))
    final = car.getWaitingTime()
    if initial < final:
        return True
    else:
        return False


# Ensure that if a car is on the road it will queue if it has a fuel value less than or equal to the threshold
def set_car_queue(car: Car, queue: list, onroad: list) -> bool:
    """
    Add a car to a queue
    :param car:
    :param queue:
    :param onroad:
    :return: True if the car state is onroad and sent to the fuel station, false otherwise

    >>> set_car_queue(Car(4,"fuelstation",1,5,5),[4],[])    # car already in the fuel station
    False

    >>> set_car_queue(Car(4,"stopped",0,5,5),[],[])         # car stopped on the road
    False

    >>> set_car_queue(Car(4,"onroad",4,5,5),[],[4])         # car on the road BELOW fuel threshold
    True

    >>> set_car_queue(Car(4,"onroad",6,5,5),[],[4])         # car on the road ABOVE fuel threshold
    False

    >>> set_car_queue(Car(4,"onroad",5,5,5),[],[4])         # car on the road AT fuel threshold
    True
    """
    if car.getState() == "onroad":
        if car.getFuel() <= car.getFuelThreshold():
            car.setState("fuelstation")
            car.resetWaitingTime()
            queue.append(car.getId())
            onroad.remove(car.getId())
            return True
    return False


# Ensure that the car ID matches the first on the list. Ensure that the car is in the "fuelstation" state.
def fill_fuel_tank(car: Car, queue: list, onroad: list) -> bool:
    """
    Fill the tank if it is the first in the queue, return the fuel in the tank.
    :param car:
    :param queue:
    :param onroad:
    :return:

    >>> fill_fuel_tank(Car(4,"onroad",5,5,5),[],[4])    # car is on the road
    False

    >>> fill_fuel_tank(Car(4,"stopped",0,5,5),[],[])    # car is stopped
    False

    >>> fill_fuel_tank(Car(4,"stopped",0,5,5),[4],[])    # car is stopped, even if by other error it is still at the top of the list
    False

    >>> fill_fuel_tank(Car(4,"fuelstation",3,5,5),[2,4],[]) # car is the queue, not at the top of
    False

    >>> fill_fuel_tank(Car(4,"fuelstation",3,5,5),[4,2],[]) # car is in the queue, at the top
    True

    >>> fill_fuel_tank(Car(4,"fuelstation",3,5,5),[3,1,6],[])    # car state is "fuelstation", but the queue list does not contain it
    False

    >>> fill_fuel_tank(Car(4,"fuelstation",3,5,5),[],[])    # car state is "fuelstation", but the queue list is empty
    False
    """
    if car.getState() == "fuelstation" and len(queue) > 0:
        if car.getId() == queue[0]:
            car.setState("onroad")
            car.setFuel(10)
            if car.getId() in queue:
                queue.remove(car.getId())
            if not (car.getId() in onroad):
                onroad.append(car.getId())
            return True
    return False


# Ensure that the car tested is not in the state "stopped"
# Ensure that the car is out of fuel only if fuel is 0
def onroad_out_of_fuel(car: Car, cars_onroad: list, cars_stopped: list) -> bool:
    """
    Test if the car is out of fuel. In that case, add car to stopped list.
    :param car:
    :param cars_onroad: cars' id on the road
    :param cars_stopped: cars' id stopped without fuel
    :return: return true if fuel is zero and car is on the road

    >>> onroad_out_of_fuel(Car(0,"stopped",0,5,5),[],[0]) # o carro já está parado
    False

    >>> onroad_out_of_fuel(Car(0,"onroad",0,5,5),[],[]) # o carro está na estrada, sem combustivel
    True

    >>> onroad_out_of_fuel(Car(0,"onroad",5,5,5),[0],[]) # o carro está na estrada, com combustivel
    False

    >>> onroad_out_of_fuel(Car(0,"fuelstation",2,5,5),[],[]) # o carro está na bomba de gasolina
    False


    """

    if car.getFuel() == 0 and car.getState() == "onroad":
        car.setState("stopped")
        if car.getId() in cars_onroad:          # para evitar excepções devido a inconsistencias nas listas
            cars_onroad.remove(car.getId())
        if not (car.getId() in cars_stopped):   # para evitar inconsistencias nas listas
            cars_stopped.append(car.getId())
        return True

    return False


def remove_car_queue(car: Car, queue: list, onroad: list, wait: int = 0) -> bool:
    """
    Test if a car in the fuel queue is above the waiting threshold
    :param wait: tempo de espera, para efeito de testes, se >= 0 não é considerado
    :param car:
    :param queue:
    :param onroad:
    :return: True if the car was removed from the queue, false otherwise

    >>> remove_car_queue(Car(0,"fuelstation",2,5,5),[],[],4) # car is in the queue, below the threshold
    False

    >>> remove_car_queue(Car(0,"fuelstation",2,5,5),[],[],5) # car is in the queue, at the threshold
    False

    >>> remove_car_queue(Car(0,"fuelstation",2,5,5),[],[],6) # car is in the queue, above the threshold
    True

    >>> remove_car_queue(Car(0,"onroad",2,5,5),[0],[],6)    # car state is onroad
    False

    >>> remove_car_queue(Car(0,"stopped",2,5,5),[0],[],6) # car state is stopped
    False

    """
    if wait > 0:    # set the car waiting time, for test purposes
        car.incrementWaitingTime(wait)
    if car.getState() == "fuelstation" and car.getWaitingTime() > car.getWaitingTimeThreshod():
        car.resetWaitingTime()          # resets the car waiting time
        car.setState("onroad")          # sets the car state
        if car.getId() in queue:
            queue.remove(car.getId())   # remove the car from the fuel queue
        if not (car.getId() in onroad):
            onroad.append(car.getId())  # add the car to the road queue
        return True

    return False


def print_car_state(car: Car):
    print("Id=", car.getId(), " Fuel=", car.getFuel(), " Fuel_Thr=", car.getFuelThreshold(), "Waiting_Time=",
          car.getWaitingTime(), "Waiting_Thr=", car.getWaitingTimeThreshod())


def main():
    # Test using doctest
    # doctest.testmod()
    # Initialization: A car is a member of the dictionary with a key (id) and the list.
    cars = dict()
    res = create_dict_cars(cars, 6, (6, 10), (0, 0), (2, 4), cars_onroad)
    print("Number of cars created:", res)
    # Cycle
    tick = 0
    while tick < MAX_TICKS:
        for id in cars:
            car = cars[id]
            reduce_fuel(car, 1)
            onroad_out_of_fuel(car, cars_onroad, cars_stopped)
            set_car_queue(car, cars_queue, cars_onroad)
            increment_waiting_time(car, 1)
            fill_fuel_tank(car, cars_queue, cars_onroad)
            remove_car_queue(car, cars_queue, cars_onroad, 0)
            # print_car_state(car)

        # Print State
        if tick % 1 == 0:
            nr_stopped = len(cars_stopped)
            nr_onroad = len(cars_onroad)
            nr_queue = len(cars_queue)
        mean_fuel = 0
        if len(cars_onroad) > 0:
            for id in cars_onroad:
                mean_fuel += cars.get(id).getFuel()
                mean_fuel = mean_fuel / len(cars_onroad)
        print("*******************")
        print("tick:", tick)
        print("Stopped:", nr_stopped)
        print("Onroad:", nr_onroad)
        print("Queue:", nr_queue)
        print("Mean fuel:", mean_fuel)
        print("*******************")
        tick += 1


main()
