class Car:
    def __init__(self, id, state, fuel, fuel_threshold, waiting_time_threshold):
        """
        inicia o estado do automóvel
        """
        self.__id = id
        self.__state = state
        self.__fuel = fuel
        self.__fuel_threshod = fuel_threshold
        self.__waiting_time_threshold = waiting_time_threshold
        self.__nr_times_bomb = 0
        self.__waiting_time = 0

    def resetWaitingTime(self):
        """
        leva para zero o valor de espera

        :return:
        """
        self.__waiting_time = 0

    def incrementWaitingTime(self, value):
        """
        Incrementa o tempo de espera
        :param value:
        :return:
        """

        self.__waiting_time += value

    def getWaitingTime(self):
        """
        Retorna o tempo de espera
        :return:
        """
        return self.__waiting_time

    def getWaitingTimeThreshod(self):
        '''retorna o tempo limite de espera na fila de combustível'''
        return self.__waiting_time_threshold

    def getId(self):
        '''retorna o id do automóvel'''
        return self.__id

    def getState(self):
        '''retorna o estado'''
        return self.__state

    def setState(self, state):
        '''atribuí um estado ao automóvel'''
        self.__state = state

    def reduceFuel(self, value):
        """reduz o nível de combustível por um valor value"""
        self.__fuel -= value

    def getFuel(self):
        """
        Retorna o nível de combustível
        :return:
        """
        return self.__fuel

    def setFuel(self, fuel):
        """
        Atribui um valor ao nível de combustível
        """
        self.__fuel = fuel

    def getFuelThreshold(self):
        """
        Retorna o limite mínimo de combustível
        :return:
        """
        return self.__fuel_threshod

    def incrementNrTimesBomb(self):
        """
        incrementa o número de vez que usou o posto de combustível
        """
        self.__nr_times_bomb += 1

    def getNrTimesBomb(self):
        return self.__nr_times_bomb

